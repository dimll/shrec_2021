dir = 'your_shape_index_files/';

opt.tsnedim = 3;
total = 1543;

bins = [-1:0.1:1];
shape = zeros(total,numel(bins)-1);

for i=1:total
    dir2 = num2str(i);
    nDir = strcat(dir,dir2,".mat");
    load(nDir);
    
    H = histogram(s, bins,'Normalization','count'); 
    shape(i,:) = H.Values;
end

shapeIdx_score = tsne(shape,'NumDimensions', opt.tsnedim);

%Save shape index score matrix
save ('shapeIdx.mat', 'shapeIdx_score');

 