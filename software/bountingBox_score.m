clear all;
close all;

%% Calculates the bounding box vertices, volume and surface for each protein structure

%Define .off files path and number of protein structures
offPath = 'shrec_21/OFF_test_files/';
numOfStructures = 1543;

for frame = 1:numOfStructures

fileName = int2str(frame);
fullfileName = strcat(offPath,fileName);

fileFormat = '.off'; 
full = strcat(fullfileName,fileFormat);
[vertex,face] = read_off(full);
vertex = vertex';
face =face';


fv.vertices= vertex;
fv.faces= face;

[newf, newv] = reducepatch(fv,0.03,'fast');

[rotmat{frame},cornerpoints{frame},volume(frame),surface(frame),edgelength(frame)] = minboundbox(newv(:,1),newv(:,2),newv(:,3));

end

% Calculate bounding box score
bb_score = zeros(numOfStructures,3);
for i = 1:size(cornerpoints,2)
    [v ,l]= eig(cornerpoints{i}'*cornerpoints{i});
    bb_score(i,1) = l(1,1);
    bb_score(i,2) = l(2,2);
    bb_score(i,3) = l(3,3);
end

%Save 
save ('boundingBox_score.mat', 'bb_score');


