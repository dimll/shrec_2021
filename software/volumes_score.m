clear all;
close all;

%% Calculates the volume of each protein structure 

%Define .off files path and number of protein structures
offPath = 'shrec_21/OFF_test_files/';
numOfStructures = 1543;

for frame = 1:numOfStructures

fileName = int2str(frame);
fullfileName = strcat(offPath, fileName);

fileFormat = '.off'; 
full = strcat(fullfileName,fileFormat);
[vertex,face] = read_off(full);

vertex = vertex';
T = delaunay(vertex(:,1),vertex(:,2));
tri = triangulation(T,vertex(:,1),vertex(:,2),vertex(:,3));

fileFormat = '_test.stl'; 
newname = strcat(fileName,fileFormat);

stlwrite(newname, face',vertex)

model = createpde;
importGeometry(model,newname);
mesh = generateMesh(model);
mv(frame,1) = volume(mesh);
mv(frame,1)
end

%Save volumes matrix (mv) 
save volumes.mat mv 
