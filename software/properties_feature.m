test = 1; % Run it the first time with test=0 and then with test=1

outpath = '../data/';
if ~exist(outpath,'dir')
    outpath = strrep(outpath,'','');
end
if ~test
    propath = '\Dataset\TXT_training_files\';
    load('labels.mat');
    numFrames = numel(labels);
    numBins = 1000;
    scorefname1 = fullfile(outpath,'score_properties_train.mat'); 
    scorefname2 = fullfile(outpath,'distances_properties_train.mat');
    
    prop = read_txt(fullfile(propath, '1.txt'));
    for ch=1:size(prop,2)  % 3
       img = squeeze(prop(:,ch));
       tmp = histogram(img(:), numBins, 'Normalization','probability');
       BinEdges(ch,:) = tmp.BinEdges;
       save('BinEdges.mat','BinEdges');
       clear tmp
    end
    
else
    propath = '\Dataset\TXT_test_files\';
    numFrames = 1543;
    load('BinEdges.mat');
    scorefname1 = fullfile(outpath,'score_properties_test.mat');
    scorefname2 = fullfile(outpath,'distances_properties.mat');  
end

%% read properties (3 per vertex) 
F = zeros(7,size(prop,2));
score = zeros(numFrames,21);
for frame = 1:numFrames
   fileName = int2str(frame);
   disp(['Frame ' fileName ]);
   %% read properties (3 per vertex) 
   prop = read_txt(fullfile(propath, [fileName,'.txt']));
   for ch=1:size(prop,2)
       X = prop(:,ch); 
%      X(isnan(X)) = 0;
       F(:,ch) = hist_stats(X,BinEdges(ch,:));
   end
   score(frame,:) = F(:); 
end

%% remove features with nan values or 0 variation
score0 = score;
score0(:,sum(isnan(score))>0)=[];
score0(:,std(score0)==0)=[];
score = score0; clear score0
save(scorefname1,'score');

if ~test
    disp('without FeatureReduction:');
    [accuracy, d, dtrue] = eval_score(score, labels, 'distance_prop_hist.mat');
    distances_properties = inverse_consistency(d, dtrue);
end
    

for dim = size(score,2)-1
    disp(['dim = ' num2str(dim)]);
    properties_score = FeatureReduction(score,labels, 'LPP', dim);
    if ~test
        [accuracy, d, dtrue] = eval_score(properties_score, labels, 'distance_prop_hist.mat');
        distances_properties = inverse_consistency(d, dtrue);
    else
        d = squareform(pdist(properties_score,'seuclidean'));
        distances_properties = inverse_consistency(d);
    end
end
save(scorefname2, 'properties_score','distances_properties');


