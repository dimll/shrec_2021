function dnew = inverse_consistency(d, varargin)
% dnew = inverse_consistency(d)
% dnew = inverse_consistency(d, dtrue)
% d    : initial distance matrix
% dtrue: you can provide an empty matrix ([]) in the test phase
% dnew : updated distance matrix that has promoted consistent neighbors

dtrue=[];
if nargin>1
    dtrue = varargin{1};
end

%% Options
maxhood = 5;     % size of neighborhood in forward mapping, i.e. N(x)
maxhood_inv = 5; % size of neighborhood in inverse mapping, i.e. N(y)
alpha = 0.3;      % amount of distance reduction if we trust this neighbor


% % %% Load data 
% % addpath('../Data/')
% % load('distance_augment_3_train.mat') % -> d
% % load('labels.mat')
% % dtrue = squareform(pdist(labels,'hamming')>0); 
% disp('Original distance matrix:'); accuracy1 = eval_distance(d, dtrue); % display baseline accuracy
  
%% For each x find nearest neighbor y, i.e. y \in N(x). 
I = zeros(size(d));
numFrames = size(d,1);
for i=1:numFrames
    [~,ind] = sort(d(i,:),'ascend');
    I(i,:) = ind;    
end
I(:,1) = [];            % it is always the protein itself
% imagesc(I(:,1:20))      % visualize only the 20 nearest neighbors

%% Then for each y find its neighbors
dnew = d;
for i=1:numFrames
    neighbor = I(i,1:maxhood);
    for n=1:maxhood
        nhood = I(neighbor(n),1:maxhood_inv);
        if any(nhood==i) % high confidence
            dnew(i,neighbor(n)) = (1-alpha)*dnew(i,neighbor(n));
        end
    end   
end

if ~isempty(dtrue)
    disp('Results after inverse consistency:'); 
    accuracy = eval_distance(dnew, dtrue);
end



