function [accuracy, varargout] = eval_score(score, labels, distfname)
% [accuracy] = eval_score(score, labels, distfname)
% [accuracy, d, dtrue] = eval_score(score, labels, distfname)
% labels:   [numFrames x 1] vector with the class labels if known (during 
%           train), or with ones if unknown (during test)
% d     :   distance matrix
% dtrue :   binarized distance matrix with zeros within class and ones
%           between clases
%
% It assesses class separability by examining the intra-class and inter-class 
% distances in relation to the feature values. 


%% dtrue is emaningful only in the training phase
dtrue = squareform(pdist(labels,'hamming')>0); % shows not equality of true class labels. 
numFrames = numel(labels); % <=size(score,1) (4 times smaller if 'augmented')

if 0 % all features at once: this gives accuracy=0.78   
    d = squareform(pdist(score,'seuclidean'));  % shows distance of predicted features

else  % check feature groups: this gives accuracy=0.835 
    numCopies = size(score,1)/numFrames; % 4
    if numCopies>1
        numFeatures = size(score,2); % e.g. 5
        allscore = reshape(score,numFrames,[]);
    %     score = zeros(size(allscore));
        score = zeros(numFrames,numFeatures,numCopies);
        % rearrange the columns as originally: this is important only if you check the maximum similarity out off all possible 4 data flips       
        for n=1:numCopies %1:4          
            for j=1:numFeatures % 1:3
                k = (j-1)*numCopies+n;
    %             score(:,(n-1)*numFeatures+j)= allscore(:, k);
                  score(:,j,n)= allscore(:, k);
    %             disp(['score(' num2str(j) ',' num2str(n) ' -> allscore(' num2str(k) ')']); 
            end
        end

        dall = nan(numFrames,numFrames,numCopies,numCopies);
        for n1=1:numCopies %1:4  
          for n2=n1:numCopies %1:4          
            dall(:,:,n1,n2) = pdist2(squeeze(score(:,:,n1)),squeeze(score(:,:,n2)),'seuclidean');  
          end        
        end
        d = min(dall,[],4);     % find the minimum distance
        d = squeeze(d(:,:,1));   % we are interested only for the 1st copy
        score = squeeze(score(:,:,1));
        save(distfname, 'd');
        
    else
        d = squareform(pdist(score,'seuclidean'));       
    end % endif numCopies>1 
end

accuracy = eval_distance(d, dtrue);
if nargout>1
     varargout{1} = d; 
end
if nargout>2
     varargout{2} = dtrue;
end
