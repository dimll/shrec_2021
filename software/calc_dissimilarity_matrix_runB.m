%Calculates the final distance matrix of run B 
% (considering geometry & physicochemical properties)

% Load the properties distance matrix
load('../data/distances_properties.mat');

%% Try 1

% Load the similiarity matrix of run A v1 
load('../results/dissimilarity_runA_1.mat');

w1 = 0.935;
w2 = 0.065;

d1 = dissimilarity_runA;
d2 = distances_properties;

dissimilarity_runB = w1*d1 + w2*d2;

%Save run B v1 dissimilarity matrix 
%save('../data/dissimilarity_runB_1.mat', 'dissimilarity_runB')

%% Try 2 
% Load the similiarity matrix of run A v2 
load('../results/dissimilarity_runA_2.mat');

w1 = 0.935;
w2 = 0.065;

d1 = dissimilarity_runA;
d2 = distances_properties;

dissimilarity_runB = w1*d1 + w2*d2;

%Save run B v2 dissimilarity matrix 
%save('../data/dissimilarity_runB_2.mat', 'dissimilarity_runB')

%% Try 3 
% Load the similiarity matrix of run A v3
load('../results/dissimilarity_runA_3.mat');

w1 = 0.925;
w2 = 0.075;

d1 = dissimilarity_runA;
d2 = distances_properties;

dissimilarity_runB = w1*d1 + w2*d2;

%Save run B v3 dissimilarity matrix 
%save('../data/dissimilarity_runB_3.mat', 'dissimilarity_runB')
