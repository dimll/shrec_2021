function [Xout, W] = FeatureReduction(X, varargin)
% function [Xout] = FeatureReduction(X)
% function [Xout, W] = FeatureReduction(X, labels, technique, d)
% 
% X                 : data matrix [N x D],  N = #points, D = dimensionality
% labels (optional) : [N x 1] class labels for display purposes
% technique (optional): dimensionality reduction technique (default: 'LPP')
% d (optional)      : intrinsic dimensionality (default: MLE estimate)
%
% Xout              : embedded data [N x d], d = embedding dimensionality (d<D)
% W                 : the computed linear basis [D x d], such that Xout=X*W 
%
% Possible dimensionality reduction techniques: 
%  [{'LDA'}, {'FactorAnalysis'}, {'GPLVM'}, {'Sammon'}, {'Isomap'}, {'LandmarkIsomap'},...
%   {'Laplacian'}, {'LLE'}, {'HessianLLE'},{'LTSA'},{ 'LLTSA'}, {'CCA'}, ...
%   {'LandmarkMVU'},{'FastMVU'}, { 'DiffusionMaps'}, {'KernelPCA'}, { 'GDA'}, { 'SNE'}, ...
%   {'SymSNE'}, {'tSNE'}, { 'LPP'}, { 'NPE'}, {'SPE'}, {'MVU'}, ...
%   {'AutoEncoderRBM'}, {'AutoEncoderEA'},{'LLC'},{'ManifoldChart'}, {'CFA'}, {'NCA'}, ...
%   {'MCML'},{'ISBI2012'}];

drpath = 'C:\Users\Eva\Dropbox (Personal)\Code\drtoolboxv_v0.8.1b';
if exist(drpath,'dir')
    addpath(genpath(drpath))
else
    error(['Dimensionality reduction toolbox not installed. Missing path: ' drpath ]); % addpath(genpath('C:\Users\Eva\Matlab\FromOthers\DimensionalityReduction\drtoolboxv_v0.7.2'))
end

[N, D] = size(X);
K = 12;                   % K = number of neighbors
             % d = embedding dimensionality
if nargin>1
    labels = varargin{1};
else
    labels = zeros(size(X,1),1);
end
if nargin>2
    drtechnique = varargin{2};
else
    drtechnique = 'LPP';
end
if nargin>3 && ~isempty(varargin{3})
    d = varargin{3};
else
    d = round(intrinsic_dim(X, 'MLE'));
    disp(['MLE estimate of intrinsic dimensionality: ' num2str(d)]);
end 
if d>=size(X,2), error('The embedding dimensionality is not smaller than the original!'); end


% fprintf(['\n ******** dimensionality reduction drtechnique :  ',drtechnique, '  ********\n']);
% [~, Xout, mapping] =  evalc('compute_mapping(X, drtechnique, d)'); % for LPP:  Xout = X * mapping.M;
% if size(X,1)<10000
    [Xout, mapping] = compute_mapping(X, drtechnique, d);
% else
%     [Xout, mapping] = compute_mapping(X, drtechnique, d, 'JDQR');
% end
% fprintf(['For ',drtechnique,':  d=',num2str(size(Xout,2)),'\n\n']);
if isfield( mapping,'M')
    W = mapping.M;
else
    W=[];
end


%%% remove columns (features) that have all values equal across subjects
ind = max(Xout)-min(Xout)<1e-010;
if any(ind)
    Xout(:,ind)=[];       %%% [N x d-1]
    W(:,ind) = [];
    disp(['The embedding dimensionality is smaller than ',num2str(d)]);
end

% Isomap/LLE/Laplacian Eigenmaps/LTSA can only embed data that gives rise to a connected neighborhood graph. 
% If the neighborhood graph is not connected, the implementations only embed the largest connected component 
% of the neighborhood graph. 
if size(Xout,1)~=N,  
    % disp([' -> The number of samples after embedding is reduced to ',num2str(size(Xout,1)),'!']);    
    mapped = zeros(N,size(Xout,2));
    ind = mapping.conn_comp;
    mapped(ind,:) = Xout;
    Xout = mapped;
end

if 0
    figure, scatter3(Xout(:,1), Xout(:,2), Xout(:,3), 5, labels); 
    figure, scatter(Xout(:,1), Xout(:,2), 21, labels); 
    legend('mapped data'); title(['Mapping with ' drtechnique]), drawnow
end

end



