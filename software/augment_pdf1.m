function [SCOREaugment_train, SCOREaugment_test, distfname] = augment_pdf1(opt)

test = opt.test;
SCOREaugment_train = [];
SCOREaugment_test = [];

load('../data/labels.mat')
numFramesTrain = numel(labels);
VVaugment_test = [];

if 1 
    VVaugment_train = flip_pdf(0, opt);
    save(['VVaugment_train.mat'],'VVaugment_train'); 
    if test
        VVaugment_test = flip_pdf(1, opt);
        save(['VVaugment_test.mat'],'VVaugment_test');
    end
else
    load('VVaugment_train.mat'); 
    if test   
        load('VVaugment_test.mat'); 
    end
end


SCOREaugment = tsne([VVaugment_test; VVaugment_train],'NumDimensions',opt.tsnedim);

if test
    numFramesTest =  size(SCOREaugment,1)/4 - numFramesTrain;
    SCOREaugment_test = SCOREaugment(1:4*numFramesTest,:);
    SCOREaugment_train = SCOREaugment(4*numFramesTest+1:end,:);
    SCOREaugment = SCOREaugment_test;  save(['data_score_augment_' num2str(opt.tsnedim) '_test.mat'],'SCOREaugment');
    SCOREaugment = SCOREaugment_train; save(['data_score_augment_' num2str(opt.tsnedim) '_train.mat'],'SCOREaugment') ;    
else
    save(['data_score_augment_' num2str(opt.tsnedim) '.mat'], 'SCOREaugment') ; % this is also trainset but without having merged features with test
end

%% Calculate distance matrix 
if test,  distfname = ['distance_augment_' num2str(opt.tsnedim) '_test.mat'];
else,     distfname = ['distance_augment_' num2str(opt.tsnedim) '_train.mat'];
end
