function d = undo_inverse_consistency(d)

for r=1:size(d,1)
    for c=1:size(d,1)
        if (r~=c)
            if (d(r,c)>d(c,r))
                d(c,r)=d(r,c);
            end
        end
    end
end

end

