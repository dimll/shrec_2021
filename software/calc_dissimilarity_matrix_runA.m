%Calculates the final distance matrix of run A 
% (considering only geometry)

% Load the pairwise distance matrices of each geometry feature
load('../data/distances_augmented.mat'); %Unfolded surface distance matrix
load('../data/distances_volume.mat'); %Volume distance matrix
load('../data/distances_bb.mat'); %Bounding box distance matrix
load('../data/distances_shapeIdx.mat'); %Shape index distance matrix

d1 = distances_augmented;
d2 = distances_volume;
d3 = distances_shapeIdx;
d4 = distances_bb;


%% Try 1
w1 = 0.22; w2 = 0.67; w3 = 0.055; w4 = 0.055;

dissimilarity_runA = w1*d1 + w2*d2 + w3*d3 + w4*d4;

%Save run A v1 dissimilarity matrix 
%save('../data/dissimilarity_runA_1.mat', 'dissimilarity_runA');

%% Try 2 
d1 = undo_inverse_consistency(d1);
w1 = 0.22; w2 = 0.67; w3 = 0.055; w4 = 0.055;

dissimilarity_runA = w1*d1 + w2*d2 + w3*d3 + w4*d4;

%Save run A v2 dissimilarity matrix 
%save('../data/dissimilarity_runA_2.mat', 'dissimilarity_runA');

%% Try 3
w1 = 0.25; w2 = 0.725; w3 = 0; w4 = 0.025;
d1 = undo_inverse_consistency(d1);

dissimilarity_runA = w1*d1 + w2*d2 + w3*d3 + w4*d4;

%Save run A v3 dissimilarity matrix 
%save('../data/dissimilarity_runA_3.mat', 'dissimilarity_runA');
