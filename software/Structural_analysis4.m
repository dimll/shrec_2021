
format shortG

%% Define input and output folders
datapath = '..\shrec_21\';
if ~exist(datapath,'dir')
    datapath = '..\shrec_21\';
end

outdir = '../Data/';
if ~exist(outdir,'dir')
    mkdir(outdir)
end
addpath(outdir);

%% Options for methods
doPlot = 0; 
useSkeletons = false; %False to utilize the off files
test = true; %True to execute it for the test set
opt.test = test;
opt.tsnedim = 5;
opt.unfold = true;
opt.augment = true;


%% Adjust parameters and filenames according to options
if test, fnamestr = '_test';
else,    fnamestr = '';
end

if useSkeletons
    fileFormat = 'obj';
    VVfname = ['data_VV' fnamestr '.mat'];
else
    fileFormat = 'off';
    if ~opt.unfold
        VVfname = ['data_pointCloud' fnamestr '.mat'];
        opt.Bandwidth = [8 8 8];
        [x1,x2,x3] = ndgrid(-32:4:32,-32:4:32,-32:4:32); % 21, samples per dimension
        x1 = x1(:,:)'; x2 = x2(:,:)'; x3 = x3(:,:)';
        pts = [x1(:) x2(:) x3(:)];       
    else
        VVfname = ['data_unfold' fnamestr '.mat'];
        opt.Bandwidth=[0.5 0.5]; 
        [x1,x2] = ndgrid(-6:0.5:6,-6:0.5:6); % 33, samples per dimension
        x1 = x1(:,:)'; x2 = x2(:,:)';
        pts = [x1(:) x2(:) ];  
    end
end
numDim = size(pts,1); % numDim = 3;
VVfname_train = strrep(VVfname,'_test',''); % if test, concatenation with the train filename will be required

%% Collect info on the training set
if ~test
    %Change the data path of the .cla file
    info = read_classTrain(fullfile([datapath,'\Dataset'],'classTraining.cla'));
    numLabels = numel(info);
    numFrames = 0; 
    maxnumMol=0;
    labels = [];
    for c=1:numLabels
        numFrames = numFrames + info{c}.numMol;
        labels = [labels; c*ones(info{c}.numMol,1)];
    end
else
    %Change the data path of the .off files
    testfiles = dir(fullfile([datapath,'\Dataset\OFF_test_files'],'*.off'));    
    numFrames =  numel(testfiles); % 1543; % the order of the filenames is 1, 10, 100, etc
end

if opt.unfold
    WW = zeros(numFrames,6);
end

    
%% Calculate data matrix VV from the point cloud for all molecules of all classes
if 0
    VV = zeros(numFrames,numDim);    
    for frame = 1:numFrames
        disp(['  Frame ' num2str(frame) ' (out of ' num2str(numFrames) ')']);
        fileName = int2str(frame);
        if useSkeletons
            if test, fullName = fullfile([datapath,'skeleton_test'], ['result_res2_21_',fileName,'.',fileFormat]); 
            else,    fullName = fullfile([datapath,'skeleton_train'], ['result_res2_21_',fileName,'.',fileFormat]); 
            end
        else
            %Change the data path of the .off train/test files
            if test, fullName = fullfile([datapath,'Dataset\OFF_test_files'], [fileName,'.',fileFormat]);
            else,    fullName = fullfile([datapath,'Dataset\OFF_training_files'], [fileName,'.',fileFormat]);
            end
        end
        try
            vertex = read_mesh(fullName); 
            vertex = vertex';
        catch
            warning(['File ' fullName ' could not be processed. The values of vertex ' int2str(frame-1) ' will be used!']);
        end
        X = bsxfun(@minus,vertex,mean(vertex));
        [coeff, score, D] = pca(X);
        if opt.unfold
            warning off
            [score, W] = FeatureReduction(score, [], 'LPP', 2);
            score = 1000*score; % just to be in a normal range and undo the scaling caused by W, e.g. for frame=1, norm(W)=0.00021
                                % Try scaling by the norm. This scaling will be different for each frame
            WW(frame,:) = W(:);
        end
        
        if  numDim==3
            covarianceMatrix = cov(X);
            [V,D] = eig(covarianceMatrix);
            VV(frame,:) = diag(D);
        elseif numDim==size(pts,1)
            f = 10^5*mvksdensity(score,pts,...
            'Bandwidth',opt.Bandwidth,...
            'Kernel','normpdf');
            if doPlot
                if size(score,2)==3 %if you want to visualize the 3D probability density
                    pdf = reshape(f,size(x1,2), size(x2,2), size(x3,2));
                    for i=1:size(pdf,3), imagesc(squeeze(pdf(:,:,i))); colorbar; pause(0.1); end
                elseif size(score,2)==2 %if you want to visualize the 2D probability density
                    pdf = reshape(f,size(x1,2), size(x2,2));
                    imagesc(pdf); colorbar; 
                end   
            end
            VV(frame,:) = f;
        else
            error(['numDim = ' num2str(numDim) ' not part of possible options']);            
        end
    end
    
    if opt.unfold,  save(VVfname,'VV','opt','WW');
    else,           save(VVfname,'VV','opt');
    end
else
    load(VVfname,'VV');
end


%% Dimensionality Reduction of VV
rmpath(genpath('drtoolboxv_v0.8.1b')); % to use tsne of Matlab and not of drtoolbox
if ~opt.augment
    % if kernel density for each molecule is used as feature vector 
    if test % concatenate with training set
        VVtest = VV; clear VV
        load(VVfname_train,'VV');
        VV = [VVtest; VV]; 
    end
    SCORE = tsne(VV,'NumDimensions',opt.tsnedim);
   
    if test
        SCORE_test = SCORE(1:numFrames,:);
        save(['data_score_' num2str(opt.tsnedim) fnamestr '.mat'],'SCORE_test');
        SCORE(1:numFrames,:) = [];
        save(['data_score_' num2str(opt.tsnedim) '_train.mat'], 'SCORE') ;
        distfname = ['distance_' num2str(opt.tsnedim) '_test.mat'];
    else
        save(['data_score_' num2str(opt.tsnedim) '.mat'], 'SCORE') ;
        distfname = ['distance_' num2str(opt.tsnedim) '_train.mat'];
    end
    
else
    
    [SCORE, SCORE_test, distfname] = augment_pdf1(opt); % Saves 'SCOREaugment' in ['data_score_augment_' num2str(opt.tsnedim) '*.mat']
end


%% Create distance matrix and then update to take into account also inverse consistency
[Accuracy, dtrain, dtrue] = eval_score(SCORE, labels, strrep(distfname,'_test','_train') );
dtrain_new = inverse_consistency(dtrain, dtrue);
save(strrep(distfname,'_test','_train'), 'dtrain_new');

if test
    [~, dtest, ~] = eval_score(SCORE_test, ones(numFrames,1), distfname);
    distances_augmented = inverse_consistency(dtest, []);
    save('distances_augmented.mat', 'distances_augmented');
end










