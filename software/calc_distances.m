% Load feature scores
load('volumes.mat')
load('boundingBox_score.mat')
load('shapeIdx.mat')

distances_volume = squareform(pdist(mv,'seuclidean'));
distances_bb = squareform(pdist(bb_score,'seuclidean'));
distances_shapeIdx = squareform(pdist(shapeIdx_score,'seuclidean'));

% Save the pairwise feature distances
save ('distances_volume.mat', 'distances_volume');
save ('distances_bb.mat', 'distances_bb');
save ('distances_shapeIdx.mat', 'distances_shapeIdx');

