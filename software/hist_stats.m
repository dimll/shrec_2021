function F = hist_stats(img,bins)
% F = histStats(img,bins)
% It extracts 7 histogram-based features for one image X from the image intesities 
%
% X   : any dimension since it will be converted to 1D vector
% bins: bin edges 


tmp = histogram(img(:), 'BinEdges',bins, 'Normalization','probability');
X = tmp.Values;
X = X/sum(X, 2); % although Normalization='probability' it does not sum-up to 1

% convert bin edges to bin centers
i=2:numel(bins);
bin_centers = 0.5*(bins(i)+bins(i-1));

%% Normalize to 1
% csum = cumsum(X,2);
% vol = csum(:,end); % for probabilities, this equals 1.
% X = X./repmat(vol, 1,size(X,2));

%% The previous division makes some values smaller to the precision accuracy => log(0)= -inf
% ind=find(abs(X(:))<eps); % equal to 0
% if ~isempty(ind)
%     X(ind) = eps; % to avoid inf when log(X)
%     if any(X(:)<eps) % i.e. if it had also negative values
%         warning('Negative histogram values?');
%     end
% end

%% 7 histogram features
intensities = repmat(bin_centers, size(X,1),1);
MLD = sum(intensities.*X, 2);
SD = sqrt(sum(((intensities - repmat(MLD, 1,size(X,2))).^2).*X,2));
skewns = sum(((intensities-repmat(MLD, 1,size(X,2))).^3).*X,2) ./ (SD.^3);
kurtss = sum(((intensities-repmat(MLD, 1,size(X,2))).^4).*X,2) ./ (SD.^4);
energy = sum(X.*X, 2);
entropy = -sum(X.*log(X), 2);

[~, indMode] =  max(X,[],2); 
modeX = (bins(indMode))';

F = [MLD SD modeX kurtss skewns energy entropy];

end


