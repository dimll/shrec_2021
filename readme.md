# SHREC 2021 Track 3.
### Retrieval and classification of protein surfaces equipped with physical & chemical properties
[Link to the track description](http://shrec.ge.imati.cnr.it/shrec21_protein/index.html)

The final dissimilarity matrices of run A and run B for the test set can be retrieved by executing [calc_dissimilarity_matrix_runA](https://gitlab.com/dimll/shrec_2021/-/blob/master/software/calc_dissimilarity_matrix_runA.m) and [calc_dissimilarity_matrix_runB](https://gitlab.com/dimll/shrec_2021/-/blob/master/software/calc_dissimilarity_matrix_runB.m) scripts respectively.
The data folder contains the pairwise distance matrix of each feature. 
The results folder contrains our dissimilarity matrices for each run. 

##### Dependencies:
[Matlab Toolbox for Dimensionality Reduction](https://lvdmaaten.github.io/drtoolbox/): To generate the distance matrix of the surface unfolding feature

For Shape Index geometrical descriptor we used [compute_matlab_matrix](https://github.com/pablogainza/masif_paper/blob/master/source/matlab_libs/compute_matlab_matrix.m) from the matlab libs of the second preprocessing phase of Masif [1] network  (https://github.com/pablogainza/masif_paper/tree/master/source/matlab_libs).

##### References:
1. Gainza P, Sverrisson F, Monti F, Rodolà E, Boscaini D, Bronstein MM, Correia BE. Deciphering interaction fingerprints from protein molecular surfaces using geometric deep learning. Nat Methods. 2020 Feb;17(2):184-192. doi: 10.1038/s41592-019-0666-6. Epub 2019 Dec 9. PMID: 31819266.

 


